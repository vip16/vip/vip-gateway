FROM golang:1.17

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

ENV GO111MODULE=on

# copy app
ADD . /app
WORKDIR /app

# handle go modules
RUN go mod download

# build
RUN go build -o build/gateway src/*.go

# server running port
EXPOSE 8751

# .keys volume
VOLUME ["/app/.keys"]

ENTRYPOINT ["/app/docker-entrypoint.sh"]
