package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
)

type Token struct {
	Id        string `json:"id"`
	Roles     string `json:"roles"`
	IssueTime int32  `json:"issueTime"`
	Ttl       int    `json:"ttl"`
	Digsig    string `json:"digsig"`
}

func verifyToken(execer string, tokenStr string) error {
	log.Printf("INFO: verify token %s", tokenStr)

	// decond
	tokenJson, err := base64.StdEncoding.DecodeString(tokenStr)
	if err != nil {
		log.Printf("ERROR: fail to base64 decode, %s", err.Error())
		return err
	}

	// unmarshal
	var token Token
	err = json.Unmarshal(tokenJson, &token)
	if err != nil {
		log.Printf("ERROR: fail to unmarshla json, %s", err.Error())
		return err
	}

	// check token user and execer
	if token.Id != execer {
		// this means token not issued for execer
		log.Printf("ERROR: mismatch token user and execer, token user %s, execer %s", token.Id, execer)
		return errors.New("Invalid auth token")
	}

	// get public key of auth
	// compose payload
	pubkey := getSenzieRsaPub(config.authRsaPub)
	payload := fmt.Sprintf("%s%s%d%d", token.Id, token.Roles, token.IssueTime, token.Ttl)

	log.Printf("INFO: verify payload %s with signarue %s", payload, token.Digsig)

	// verify token
	err = verify(payload, token.Digsig, pubkey)
	if err != nil {
		log.Printf("ERROR: cannot verify token signarue")
		return err
	}

	// check expire time
	now := timestampUnix()
	expire := token.IssueTime + int32(token.Ttl*60)
	log.Printf("INFO: now %d, ttl %d", now, expire)
	if now > expire {
		log.Printf("ERROR: expired auth token")
		return errors.New("Expired auth token")
	}

	return nil
}
